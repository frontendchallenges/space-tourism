import { createWebHistory, createRouter } from "vue-router";
import SpaceComponent from "@/components/SpaceComponent.vue";
import DestinationComponent from "@/components/DestinationComponent.vue";
import CrewComponent from "@/components/CrewComponent.vue";
import TechnologyComponent from "@/components/TechnologyComponent.vue";
import NotFoundComponent from "@/components/NotFoundComponent.vue";

const routes = [
  {
    path: "/",
    redirect: { name: 'Space' }
  },
  {
    path: "/space",
    name: "Space",
    component: SpaceComponent,
  },
  {
    path: "/destination/:id",
    name: "Destination",
    component: DestinationComponent,
    props: true
  }, {
    path: "/crew/:id",
    name: "Crew",
    component: CrewComponent,
    props: true
  },
  {
    path: "/technology/:id",
    name: "Technology",
    component: TechnologyComponent,
    props: true
  },
  {
    path: "/:pathMatch(.*)",
    component: NotFoundComponent,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;