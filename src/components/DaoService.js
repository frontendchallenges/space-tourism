import data from "@/assets/data.json";

const fetchData = (item, id) => {
    return data[item][id];
};

export const fetchDestinationById = (id) => fetchData("destinations", id);
export const fetchCrewById = (id) => fetchData("crew", id);
export const fetchTechnologyById = (id) => fetchData("technology", id);
